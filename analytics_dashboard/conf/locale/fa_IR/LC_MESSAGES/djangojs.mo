��    <      �  S   �      (     )     1     5     O     S     c     j     }     �     �     �     �     �     �               "     5     :     F     \  
   s     ~     �     �     �  	   �     �     �  
   �     �     �     �     �                    "  
   3     >     Q     Z     g  4   o     �     �     �     �     �     �     �     �     �            	   *     4     9     @  �  P  
   �	      
     
     
     #
     5
     E
     [
  *   h
     �
  *   �
      �
  <   �
     9     U     n     w     �     �     �      �     �     �               )     E  "   S     v      �     �     �     �     �     �  (   �           )     <     E     \     i     y  G   �     �     �  
   �      �  &        E     R     d     s     �     �     �     �     �     �         (   3      .   &           "              0          2                        $         *   	         )   7   :              /   4                  1   
      8      ,   +       !   6             <                9   ;   #                     -                    '   5          %                 (empty) ... <%=value%> (<%=percent%>) Age Age: <%=value%> Answer Answer: <%=value%> Audit Average Complete Views Average Correct Average Incomplete Views Average Incorrect Average Submissions Per Problem Complete Views Completion Percentage Correct Current Enrollment Date Discussions Education: <%=value%> Educational Background Enrollment Female Gender: <%=value%> Honor Incomplete Views Incorrect Learner Details Learners Loading... Male Name (Username) Not Reported Order Other Page Not Found Percent Percent of Total Percentage Percentage Correct Problems Professional Replays Sorry, we couldn't find the page you're looking for. Submission Count Time Total Tried a Problem Unique Viewers Variant Verified Videos Watched a Video Week Ending Week Ending <%=value%> next page page search sort descending Project-Id-Version: edx-platform
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-03-16 11:31-0400
PO-Revision-Date: 2017-04-08 19:07+0000
Last-Translator: Ned Batchelder <ned@edx.org>
Language-Team: Persian (Iran) (http://www.transifex.com/open-edx/edx-platform/language/fa_IR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fa_IR
Plural-Forms: nplurals=1; plural=0;
 (خالی) ... <%=value%> (<%=percent%>) سن سن : <%=value%> پاسخ بده پاسخ:  <%=value%> رسیدگی میانگین بازدیدهای کامل میانگین درست ها میانگین بازدیدهای ناقص میانگین نادرست ها میانگین ارائه ها به ازای هر مسئله بازدیدهای کامل درصد تمام شدن صحیح ثبت نام جاری تاریخ  بحث و گفتگو تحصیلات: <%=value%> پیش زمینه تحصیلات  ثبت‌نام زن جنسیت: <%=value%> افتخارات بازدیدهای ناقص نا درست جزئیات دانش آموزان دانش آموزان در حال بارگذاری ... مرد نام (نام کاربری) گزارش نشده دیگر غیره صفحه مورد نظر یافت نشد درصد درصد از کل درصد درصد درست ها مشکلات حرفه ایی بازپخش ها با عرض پوزش، صفحه مورد نظر شما یافت نشد. تعداد تحویل زمان تمامی یک مشکل را حل کردم بازدیدکننده های ویژه متغییر تایید شده ویدیوها یک فیلم دیدم پایان هفته پایان هفته <%=value%> صفحه بعدی صفحه جستجو کن مرتب سازی نزولی 