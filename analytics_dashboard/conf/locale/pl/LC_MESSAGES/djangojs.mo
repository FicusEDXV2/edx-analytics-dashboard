��    =        S   �      8     9     A     E     I     Y     `     }     �     �     �     �     �     �     �     �     �     �     
  
   !     ,     <     N     U     c     v  	   |     �  
   �     �     �     �  1   �     �     �     �     �               '     6     C     L  4   _     �     �     �     �     �     �     �     �     �     �            	   (     2     @     G     V  0  f     �
     �
     �
     �
  
   �
     �
     �
  	   �
     �
     �
               (     -     C     L     X     s  
   �     �     �     �     �     �     �     �     �     �     �            =   !  
   _     j     o     �     �     �     �     �  
   �     �  @   �     ;     @     E  	   ]     g     v     �     �     �     �     �     �     �               %     5         2   :       '   %   !                     .                    ,              #   =      *         +       6       7          -   1       
          /         0         4      )                    <              9   ;   "                  8         5   (      	   &   3         $                  (empty) ... Age Age: <%=value%> Answer Attempts per Problem Correct Audit Clear Cohort Correct Country or Region Current Enrollment Date Discussion Contributions Discussions Download CSV Education: <%=value%> Educational Background Enrollment Enrollment Mode Enrollment Tracks Female Find a course Gender: <%=value%> Honor Incorrect Learners Loading... Male N/A Not Reported Number of unique problems this learner attempted. Order Other Page Not Found Percent Percent of Total Problems Correct Problems Tried Professional Segments Segments to Ignore Sorry, we couldn't find the page you're looking for. Time Total Tried a Problem Value Verified Verified Enrollment Videos Videos Viewed Watched a Video Week Ending Week Ending <%=value%> click to sort next page previous page search sort ascending sort descending Project-Id-Version: edx-platform
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-03-16 11:31-0400
PO-Revision-Date: 2017-04-11 15:42+0000
Last-Translator: Ned Batchelder <ned@edx.org>
Language-Team: Polish (http://www.transifex.com/open-edx/edx-platform/language/pl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pl
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>=14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);
 (puste) ... Wiek Wiek: <%=value%> Odpowiedź Prób do udanych ćwiczeń Przeglądający Wyczyść Grupa Dobrze Kraj lub Region Obecna rekrutacja Data Uczestników dyskusji Dyskusji Pobierz CSV Wykształcenie: <%=value%> Edukacyjne tło Rekrutacja Tryb rekrutacji Ścieżki kursu Kobieta Znajdź kurs Płeć: <%=value%> Honor Źle Studenci Wczytywanie... Mężczyzna Nie Dotyczy Nie raportowane Liczba unikalnych ćwiczeń do których przystąpił student. Zamówione Inny Nie znaleziono strony Procent Procent całości Podejść poprawnych Podejść do ćwiczeń Zawodowy Segmentów Segmentów do zignorowania Przepraszamy, nie udało się odnaleźć strony której szukasz. Czas Suma Podeszło do ćwiczenia Wartość Zweryfikowany  Ścieżka potwierdzona Filmów Obejrzeń filmów Obejrzało film Koniec tygodnia koniec tygodnia <%=value%> kliknij, aby posortować następna strona poprzednia strona szukaj sortuj rosnąco sortuj malejąco 